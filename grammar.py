# -*- coding: utf-8 -*-
"""\
This module contains the definition of TAGrammar and TAGEntries
objects. It provides various facilities such as tree to svg conversion
or tree to tikz conversion (using the tikz2svg python module).
Author: Yannick Parmentier
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pickle, svgwrite, sys, json
from   copy      import deepcopy
from   subcat    import SubCat
from   jsonproc  import JSONPathSearch
from   queryproc import QueryProcessor

config = {
    'NODEWIDTH' : 120, 
    'NODEHEIGHT':  60,
    'TEXTWIDTH' :  40,
    'LINEHEIGHT':  12,
    'TOPSHIFT'  :  20
    }

class TAGrammar(object):

    def __init__(self):
        self._entries = {} #dict name-entry
        self._families= [] #list of str (for display)
        self._indexed = {} #dict family-entries
        self._subcats = {} #for storing subcat frames for the entire grammar
        self._size    =  0 #grammar size (for e.g. progress bars)

    @property
    def entries(self):
        return self._entries
    
    @property
    def families(self):
        return self._families

    @property
    def indexed(self):
        return self._indexed

    @property
    def size(self):
        return self._size

    def addEntry(self, entry):
        self._entries[entry.name] = entry #entry name must be unique
        if entry.family in self._indexed.keys():
            self._indexed[entry.family].append(entry)
        else:
            self._indexed[entry.family] = [entry]
        if entry.family not in self._families:
            self._families.append(entry.family)

    def getentry(self, name):
        return self._entries[name]

    def filter(self, crit, subgrammar, progload):
        '''filter grammar according to class name(s) or other criteria (words in the yield, or tiger search-like queries)'''
        criteria  = crit.strip()
        querytype = criteria[:2] #first two char define the type of query (c:,w:,t:)
        if querytype not in ['c:','w:','t:']:
            print('Unknown query type, please check your query', file=sys.stderr)
        else:
            crit2 = [x.lower() for x in criteria[2:].split(';')]
            for e in self._entries.keys():
                if progload is not None:
                    progload.value += 1
                    if 'printProgressBar' in dir(progload):
                        progload.printProgressBar()
                entry = self._entries[e]
                if querytype == 'c:': #search by class in traces
                    trace2 = [x.lower() for x in entry.trace]
                    if not set(crit2).isdisjoint(set(trace2)):
                        subgrammar.addEntry(entry)
                elif querytype == 'w:': #search on projections (search by words)
                    proj = []
                    entry.getprojection(entry.tree, proj, [])
                    proj2 = [x.lower() for x in proj]
                    if not set(crit2).isdisjoint(set(proj2)):
                        subgrammar.addEntry(entry)
                elif querytype == 't:': #search on trees (tigerSearch-like query)
                    query     = criteria[2:]
                    jsonentry = entry2json(entry)
                    q = QueryProcessor()
                    try:
                        q.parseQuery(query)
                    except Exception as e:
                        print('Ill-formed query, please check your query', file=sys.stderr)
                        print(e)
                    accu = q.accu #query translated into jsonpath set of constraints
                    jsonsearch= JSONPathSearch(accu)
                    if jsonsearch.processQueryEntry(jsonentry):
                        subgrammar.addEntry(entry)
                    
    def setSize(self): #used to initialize progress bars
        self._size = len(self._entries.keys())
                
    def log(self, filename, progload):
        logfile = open(filename, 'w')
        headtab = {}    #head percolation table
        tagset  = set() #tagset(!)
        sent2dupcats = {} #sent id <-> duplicated cats
        dupcat2sents = {} #dup cat <-> sent ids
        cat2mothers  = {} #cat <-> mother cats
        cat2siblings = {} #cat <-> siblings
        for e in self._entries.keys():
            entry = self._entries[e]
            if progload is not None:
                progload.value += 1
                if 'printProgressBar' in dir(progload):
                    progload.printProgressBar()
            #print('Processing ' + entry.name) #debug only
            sentId  = entry.name
            feedSubCat(self._subcats, entry.tree, tagset, sentId, sent2dupcats, dupcat2sents, cat2mothers, cat2siblings)
            #change set to lists for json export:
            set2list(sent2dupcats) #sent is done
        #(set to list change continued, now all entries are processed)
        set2list(dupcat2sents)
        set2list(cat2mothers)
        set2list(cat2siblings)
        #compute head percolation table
        for sc in sorted(self._subcats.keys()):
            s = str(self._subcats[sc])
            #print(s) #debug only
            logfile.write(s)
            logfile.write('\n')
            s2=str(self._subcats[sc].getCandidateHead(headtab))
            logfile.write(s2)
            logfile.write('\n')
        logfile.close()
        #generate json output files
        with open(filename[:-4]+'-headtable.json', 'w') as outfile:  
            json.dump(headtab, outfile, indent=4, sort_keys=True)
        with open(filename[:-4]+'-tagset.json', 'w') as outfile:  
            json.dump(sorted(list(tagset)), outfile, indent=4, sort_keys=True)
        with open(filename[:-4]+'-sent2dup.json', 'w') as outfile:
            json.dump(sent2dupcats, outfile, indent=4, sort_keys=True)
        with open(filename[:-4]+'-dup2sent.json', 'w') as outfile:
            d = sorted(dupcat2sents.items(), key=lambda x: len(x[1]), reverse=True)
            json.dump(d, outfile, indent=4, sort_keys=False)            
        with open(filename[:-4]+'-siblings.json', 'w') as outfile:
            json.dump((cat2mothers,cat2siblings), outfile, indent=4, sort_keys=True)

    def latex(self, filename, progload, maxsize):
        for e in self._entries.keys():
            entry = self._entries[e]
            if progload is not None:
                progload.value += 1
                if 'printProgressBar' in dir(progload):
                    progload.printProgressBar()
            #print('Processing ' + entry.name) #debug only
            projection = []
            entry.getprojection(entry.tree, projection, [])
            if len(projection) <= maxsize:
                treesvg  = entry.tree2svg(False,[],[]) #no feature
                entry.treesvg2tikz(treesvg, filename+'_'+entry.name+'.tex')

    def grammar2json(self, filename, progload):
        res = dict()
        res['file']    = filename
        res['entries'] = list()
        res2= dict()
        res2['sentences'] = list()
        for e in self._entries.keys():
            entry = self._entries[e]
            if progload is not None:
                progload.value += 1
                if 'printProgressBar' in dir(progload):
                    progload.printProgressBar()
            #print('Processing ' + str(entry)) #debug only
            res['entries'].append(entry2json(entry))
            res2['sentences'].append(entry2mini(entry))
        with open(filename + '.json', 'w', encoding='utf8') as outfile:
            json.dump(res, outfile, indent=4)
        with open(filename + '.txt.json', 'w', encoding='utf8') as outfile2:
            json.dump(res2, outfile2, indent=4, ensure_ascii=False)

    def grammar2trees(self, filename, progload):
        #used to extract trees from a treebank
        #1. get headtable
        try:
            with open(filename + '-headtable.json', 'r') as infile:
                headtable = json.load(infile)
            #print(headtable) #debug only
        except FileNotFoundError as e:
            print('head percolation table missing, stop', file=sys.stderr)
            sys.exit(1)
        #2. process trees
        for e in self._entries.keys():
            entry = self._entries[e]
            if progload is not None:
                progload.value += 1
                if 'printProgressBar' in dir(progload):
                    progload.printProgressBar()
            #print('Processing ' + str(entry)) #debug only
            percolate(entry.tree, headtable)
        #3. save result
        self.save(filename + '-extracted.json')
            
    def save(self, filename):
        output = open(filename, 'wb')
        pickle.dump((self._entries, self._families, self._indexed), output)

    def load(self, filename):
        pkl_file = open(filename, 'rb')
        self._entries, self._families, self._indexed = pickle.load(pkl_file)

        
class TAGEntry(object):
    
    def __init__(self, tname, tfamily, ttrace, ttree, tsemantics, tiface):
        """
        Return a TAG entry made of a name, a trace, a syntactic tree, a semantic representation and an syn/sem interface
        """
        self.__name      = tname       #str
        self.__family    = tfamily     #str
        self.__trace     = ttrace      #list of str
        self.__tree      = ttree       #Node (see loadXML module)
        self.__semantics = tsemantics  #str
        self.__iface     = tiface      #FS   (see loadXML module)

    @property
    def family(self):
        return self.__family

    @property
    def name(self):
        return self.__name

    @property
    def trace(self):
        return self.__trace

    @property
    def tree(self):
        return self.__tree

    @property
    def semantics(self):
        return self.__semantics

    @property
    def iface(self):
        return self.__iface

    def __repr__(self):
        return self.__name + ' ' + self.__family + ' ' + ' '.join(self.__trace) + ' ' + str(self.__tree) + '(' + str(self.treeheight(self.__tree)) + ',' +  str(self.nbleaves(self.__tree)) + ')' + ' ' + str(self.__semantics) + ' ' + str(self.__iface)


    def getprojection(self, node, proj, proj2):
        '''computes projection (ordered list of leaves)'''
        nbchild = node.childCount()
        if nbchild == 0:
            proj.append(node.cat())
        elif nbchild == 1:
            #print(node.fs.table.keys())
            #print(node.name)
            if node.name == 'w':
                if'lemma' in node.fs.table:
                    lemma = node.fs.table['lemma']
                    word  = node.child(0).cat()
                else:
                    lemma = node.fs.table['catint']
                    word  = node.child(0).cat()
                proj2.append((word,lemma))                    
            self.getprojection(node.child(0), proj, proj2)
        else:
            for i in range(nbchild):
                self.getprojection(node.child(i), proj, proj2)

                
    def treeheight(self, node):
        '''computes (sub)tree height'''
        nbchild = node.childCount()
        if nbchild > 0:
            listH=[]
            for i in range(nbchild):
                listH.append(self.treeheight(node.child(i)))
            return 1 + max(listH)
        else:
            return 1

        
    def nbleaves(self, node):
        '''computes total number of leave nodes'''
        nbchild = node.childCount()
        if nbchild == 0:
            return 1
        else:
            nb = 0
            for i in range(nbchild):
                nb += self.nbleaves(node.child(i))
            return nb

        
    def tree2svg(self, featOn, feats, ffilter):
        '''computes SVG encoding of a tree with or w/out features + feature(s) hightlighting and filtering'''
        #0.Enlarge node size if featOn is set to True
        if featOn:
            config['NODEWIDTH'] = config['NODEWIDTH'] * 2
            config['NODEHEIGHT']= config['NODEHEIGHT']* 3
        #Prepare printable area size: 
        w = self.nbleaves(self.__tree)   * config['NODEWIDTH']
        h = self.treeheight(self.__tree) * config['NODEHEIGHT']
        #Create svg XML doc:
        svg_doc = svgwrite.Drawing(size = (str(w)+'px', str(h)+'px'), debug = True)#, profile='tiny')
        #White background
        svg_doc.add(svg_doc.rect(insert=(0, 0), size=('100%', '100%'), fill='white'))
        #Defs for text background (no longer needed, left for documentation)
        #fil = svgwrite.filters.Filter(start=(0,0),size=(1,1),id_='solid')
        #fil.feFlood(flood_color='white')
        #fil.feComposite(in_='SourceGraphic')
        #svg_doc.defs.add(fil)
        #Recursively process tree:
        self.populateSVG(svg_doc, featOn, 0, 0, w, w//2, config['TOPSHIFT'], self.__tree, None, None, feats, ffilter)
        #reinit
        if featOn:
            config['NODEWIDTH'] = config['NODEWIDTH'] // 2
            config['NODEHEIGHT']= config['NODEHEIGHT']// 3
        #return svg string        
        return svg_doc.tostring()

            
    def populateSVG(self, svg_doc, featOn, shiftx, shifty, w, posx, posy, node, parent, parentColor, feats, ffilter):
        '''computes (sub)tree svg representation

        featOn is a boolean indicating whether features are to be displayed,
        shiftx, shifty are coordinates of the top-left corner of the window used to display the (sub)tree
        w is the window width,
        posx, posy are coordinates of the root node of the (sub)tree,
        parent is a couple (posxp, posyp) of coordinates if there is any and None otherwise,
        feats is a list of feature values which have been selected by users (to be displayed in red)
        ffilter is a list of feature names (to restrict the features to be displayed)
        '''

        #0. Precompute feature text height (for recursive calls)
        text = '' 
        if featOn:
            text += node.fs.prettyprint(node.fs) #str(node.fs)
            #print(text)
        lines = text.split(';')
        #print(lines)
        flines= [] 
        if len(ffilter) > 0: #filtering needed
            for li in lines:
                if li != '':
                    if li.split(':')[0].strip() in ffilter: #to keep
                        flines.append(li)
                    if li.split(':')[0].strip() in ['top', 'bot']: #to keep
                        flines.append(li)
                else:
                    flines.append(li) #to preserve the right spacing    
        else:
            flines = lines
        #print(flines)
        featshifty = (len(flines) + 1) * config['LINEHEIGHT'] #+1 factor to avoid overwriting edges
        #print(str(node.fs) + ' ' + str(featshifty)) #debug only

        #1. Draw lines if any (done first for better display cf white background for nodes)
        #init shifts (hor + ver)
        allshiftx = shiftx               #left-most daughter node
        allshifty = config['NODEHEIGHT'] #idem
        if parent != None:
            color = 'black'
            if parentColor == node.color: #update line color, cf basicDiff
                color = parentColor
            svg_doc.add(svg_doc.line( (parent[0],parent[1]+shifty), (posx,posy) , stroke=color, stroke_width='1') )

        #2. Recursively process tree
        for i in range(node.childCount()):
            winwidth = (w//self.nbleaves(node))*self.nbleaves(node.child(i))
            self.populateSVG(svg_doc, featOn, allshiftx, featshifty, winwidth, allshiftx + winwidth//2, posy+allshifty, node.child(i), (posx,posy), node.color, feats, ffilter)
            allshiftx += winwidth

        #3. Create group to host node (rect + text)
        group = svg_doc.g()
        textheight = config['TEXTWIDTH']  #put in a dedicated variable to highlight it in case we want to modify it later

        #Add rect where to put node info
        group.add(svg_doc.rect(insert=(posx-(config['TEXTWIDTH']//2),posy-(config['TEXTWIDTH']//2)), size=(config['TEXTWIDTH'], textheight), fill='white'))
        #Add node cat
        label = node.cat().upper() if node.ntype not in ['lex', 'flex'] else node.cat()
        label+= '_' + node.head[:3] if hasattr(node, 'head') else '' #add a plus sign if it is a head
        svg_text = svg_doc.text(label + node.typerepr(), insert=(posx,posy), text_anchor='middle', alignment_baseline='central', font_size='18px', fill=node.color)#, filter='url(#solid)') #filter applying white background to text no longer needed, left for documentation
        #Add empty line
        tspan0 = svgwrite.text.TSpan('\t\t\t', x=[str(posx-(config['TEXTWIDTH']//2))], dy=[str(config['LINEHEIGHT'])], font_size='6px', text_anchor='start')
        tspan0.__setitem__('xml:space','preserve')
        svg_text.add(tspan0)
        #Add node name
        if node.name is not None and node.name != '':
            tspanName = svgwrite.text.TSpan(node.name, x=[str(posx-(config['TEXTWIDTH']//2))], dy=[str(config['LINEHEIGHT'])], font_size='10px', text_anchor='start', fill='green')
            tspanName.__setitem__('xml:space','preserve')
            tspanName.__setitem__('style','font-weight: bold;')
            tspanName.__setitem__('textLength',config['TEXTWIDTH'])
            tspanName.__setitem__('lengthAdjust','spacing')
            svg_text.add(tspanName)
            svg_text.add(tspan0)
        #Add node features
        for l in flines:
            if l != '':
                val = l.split(':')[1]
                if val in feats: #displayed in red
                    tspan = svgwrite.text.TSpan(l, x=[str(posx-(config['TEXTWIDTH']//2))], dy=[str(config['LINEHEIGHT'])], font_size='10px', text_anchor='start', fill='red')
                else: #displayed in blue
                    tspan = svgwrite.text.TSpan(l, x=[str(posx-(config['TEXTWIDTH']//2))], dy=[str(config['LINEHEIGHT'])], font_size='10px', text_anchor='start', fill='blue')
                tspan.__setitem__('xml:space','preserve')
                tspan.__setitem__('style','font-weight: bold;')
                tspan.__setitem__('textLength',config['TEXTWIDTH'])
                tspan.__setitem__('lengthAdjust','spacing')
                svg_text.add(tspan)
        group.add(svg_text)
        svg_doc.add(group)

        
    def treesvg2tikz(self, svgstr, texpath):
        sys.argv = [] #to avoid conflict with svg2tikz's Optparse
        import svg2tikz
        svg=svgstr.replace('\n', '').replace('encoding=\"utf-8\"', '').replace(u'\u2605','$+star$').replace(u'\u2666','$+diamond$').replace(u'\u25c7', '$+diamondsuit$').replace(u'\u2193', '$+downarrow$').replace(u'\u266f', '#').replace('|','$+mid$')
        #print(svg)
        #if you do not want standelone tex file, add codeoutput="codeonly" to convert_svg args
        tikzcode  = '''\\documentclass[tikz, margin=5mm]{standalone}
\\usepackage[utf8]{inputenc}
\\usepackage{tikz}
\\begin{document}'''
        tikzcode += svg2tikz.convert_svg(svg, codeoutput="figonly").replace('\\$+','$\\').replace('\\$','$')
        tikzcode +='\\end{document}'
        #print(tikzcode)
        texfile = open(texpath, 'w')
        texfile.write(tikzcode)
        texfile.close()


    def basicDiff(self, entry2):
        #We copy self to safely modify it:
        entry = TAGEntry(self.__name, self.__family, self.__trace, deepcopy(self.__tree), self.__semantics, self.__iface)
        #We modify deep copy of tree1
        self.diff(entry.tree, entry2.tree)
        return entry

    
    def diff(self, tree1, tree2):
        if tree1.cat() == tree2.cat(): #for now, we only compare cat
            k = 0
            for i in range(len(tree2.children)):
                if k >= len(tree1.children):
                    child2 = deepcopy(tree2.children[i])
                    tree1._children += (child2,)
                    child2.subColor('orange')
                self.diff(tree1.children[k], tree2.children[i])
                k += 1
            if k < len(tree1.children):
                for j in range(k,len(tree1.children)):
                    tree1.children[j].subColor('orange')
        else:
            tree1.subColor('orange')

            
###################################################        
#Utility functions (not part of the TAGEntry class)        
def feedSubCat(scatdict, node, tagset, sent, sent2dupcats, dupcat2sents, cat2mothers, cat2siblings):
    '''Function used to extract the subcat frames from a (sub)tree
    '''
    #Update tagset
    tagset.add(node.cat())
    if node.ntype != 'lex' and node.childCount() > 0: #we do not store (pre)terminal symbols
        #Get mother category:
        mCat = node.cat()
        #print(mCat) #debug only
        #Get SubCat object:
        if mCat not in scatdict.keys():
            scatdict[mCat] = SubCat(mCat)
        sCat = scatdict[mCat]
        #Get children categories:
        cCat  = list(map(cat,node.children))
        allCat= [x for x in cCat if x not in ['PONCT','-', '']]
        #print(cCat) #debug only
        #Update subcat occurrence: 
        sCat.addOccu(allCat)
        #Keep track of duplicates:
        if len(allCat) != len(set(allCat)):
            duplicates = set([x for x in allCat if allCat.count(x) > 1])
            if sent not in sent2dupcats.keys():
                sent2dupcats[sent] = duplicates 
            else:
                sent2dupcats[sent] = sent2dupcats[sent] | duplicates #union
            for dcat in duplicates:
                updateset(dupcat2sents, dcat, sent)
        #Update dicts about siblings
        for icat in range(len(allCat)):
            c = allCat[icat]
            updateset(cat2mothers, c, mCat)
            siblingsleft = allCat[:(icat-1)]
            siblingsright= allCat[icat:]
            sep1 = '' if len(siblingsleft)  == 0 else ';'
            sep2 = '' if len(siblingsright) == 0 else ';'
            allsiblings  = ';'.join(siblingsleft) + sep1 + '_' + sep2 + ';'.join(siblingsright) + '[' + mCat + ']'
            updateset(cat2siblings, c, allsiblings)
        #Recursive tree traversal:
        for i in range(node.childCount()):
            feedSubCat(scatdict, node.child(i), tagset, sent, sent2dupcats, dupcat2sents, cat2mothers, cat2siblings)


def cat(node): #used by map in the feedSubCat function
    return node.cat()


def updateset(adict, key, val):
    if key not in adict.keys():
        adict[key] = { val }
    else:
        adict[key] = adict[key] | { val }


def set2list(adict):
    for k in adict.keys():
        adict[k] = sorted(list(adict[k]))

        
########## Minimal encoding #########
def entry2mini(obj):
    res = dict()
    num   = obj.name.split('_')[0]
    fdesc = obj.name.split('_')[-1].split('.')[0]
    res['id'] = fdesc + '-' + num
    proj = []
    proj2= []
    obj.getprojection(obj.tree, proj, proj2)
    res['sent']   = ' '.join(proj)
    res['tokens'] = list(map(lambda x: (x[0],x[1]), proj2))
    return res


########## JSON encoding #########
def entry2json(obj):
    res = dict()
    res['entry_id']  = obj.name
    res['family']    = obj.family
    res['trace']     = obj.trace
    res['semantics'] = sem2json(obj.semantics)
    res['iface']     = fs2json(obj.iface)
    res['tree']      = node2json(obj.tree)
    return res

def node2json(node):
    # Utility recursive function for JSON encoding
    res = dict()
    if hasattr(node, 'head'):
        res['head']= node.head
    if hasattr(node, 'name'):
        res['name']= node.name
    res['ntype']   = node.ntype
    res['fs']      = fs2json(node.fs)
    res['children']= list(map(node2json,node.children))
    return res

def fs2json(fs):
    # Utility recursive function for JSON encoding
    if fs is not None:
        if hasattr(fs,'table'):
            res = {}
            for k,v in fs.table.items():
                res[k] = fs2json(v)
            return res
        else:
            return fs
    else:
        return None

def sem2json(sem):
    res = []
    for lit in sem:
        res.append(str(lit))
    return res


########## Grammar extraction #########
def percolate(node, headtable):
    #print(node.cat() + str(len(node.children)))
    if len(node.children) == 0:
        setattr(node, 'head', node.cat())
    elif len(node.children) == 1:
        percolate(node.children[0], headtable)
        setattr(node, 'head', node.children[0].head)
    else: #more than one child
        # make sure all daughter nodes have a head feature
        for x in node.children:
            percolate(x,headtable)
        #find head value to be percolated to the mother
        cat        = node.cat()
        subcat     = list(map(lambda x : x.cat(), node.children))
        priorities = list(map(lambda x : x[0], headtable[cat])) if cat in headtable.keys() else []
        #print(cat + '-' + str(subcat))
        for c in priorities:
            if c in subcat:
                pos = subcat.index(c)
                setattr(node, 'head', node.children[pos].head)
                #print('selecting ' + c + ' as head for ' + cat)
                break
        if not(hasattr(node, 'head')):
            # priorities list is empty or its intersection with subcat is empty
            setattr(node, 'head', node.cat()) #default for debugging
