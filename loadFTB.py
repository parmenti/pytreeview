# -*- coding: utf-8 -*-
"""\ 
This module reads syntactic trees encoded in XML format from the French TreeBank.
It outputs objects of type TAGEntries so as to be displayed by pytreeview. 
Author: Yannick Parmentier 
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import json, sys, os
from grammar  import TAGEntry
from lxml     import etree

class FTBloader(object):
    
    def __init__(self, filename, encoding):
        self.xmlfile = filename
        self.encoding= encoding
        self.suffix  = 1 #for creating unique names

    def getGrammar(self, g, progload=None):
        parser     = etree.XMLParser(remove_comments=True, encoding=self.encoding)
        try:
            xmldoc = etree.parse(self.xmlfile, parser = parser)
            #nbSent = xmldoc.xpath('count(//SENT[@nb=1000])') #test only
            nbSent = xmldoc.xpath('count(//SENT)')
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbSent)
                else: #pyForms bar
                    progload.max = nbSent
            #for sentence in xmldoc.xpath("/text/SENT[@nb=1000]"): #test only 
            for sentence in xmldoc.xpath("/text/SENT"): 
            #for _, sentence in etree.iterparse(self.xmlfile, events=('start',),tag='SENT'): #unused, left for documentation
                tage = self.getEntry(sentence)  #tage here is a full syntactic tree
                if progload is not None:
                    if 'printProgressBar' in dir(progload):
                        progload.value += 1
                        progload.printProgressBar()
                    else: #pyForms bar
                        progload.value += 1
                g.addEntry(tage)
            #    sentence.clear()  # -> caused python3's core dumped error 
        except etree.XMLSyntaxError:
            raise Exception("XML syntax error")

    def getEntry(self,xmlentry):
        tname      = xmlentry.get('nb') + '_' + str(self.suffix) + '_' + os.path.splitext(os.path.basename(self.xmlfile))[0]
        tfamily    = 'TB'
        ttrace     = []
        tsemantics = ''
        xmlfs      = None 
        tiface     = None 
        ttree      = Node(xmlentry)
        self.suffix= self.suffix + 1
        return TAGEntry(tname,tfamily,ttrace,ttree,tsemantics,tiface)

class Node(object):

    def __init__(self, xmlnode):
        self._color = 'black' #for tree diff
        self._name  = str(xmlnode.tag)
        self._ntype = 'lex' if self._name in ['w','lex'] else 'std'
        fs = []
        if xmlnode.items() is not None: #internal nodes are only made of a cat-based tag (no attribute) 
            fs = xmlnode.items()
        else:
            fs = [('cat',self._name)] 
        self._fs = FS(fs)
        if self._name == 'w' and len(list(xmlnode)) == 0: #leaf w node, the lexical item is in the node's text (we create a new leaf for it)
            self._children = tuple([Node(etree.Element('lex', cat=json.dumps(xmlnode.text, ensure_ascii=False).strip('"')))]) #dumps is used to process quotes, lex is used as a tag cause w would cause infinite looping
        else:
            self._children = tuple(map(Node,list(xmlnode)))
            
    def cat(self):
        nodeCat = self._name
        if self._name in ['w','lex']:
            try:
                nodeCat = str(self._fs.table['cat'])
            except KeyError:
                try:
                    nodeCat = str(self._fs.table['catint']) #for internal w nodes in XML (i.e. MWEs)
                except KeyError:
                    sys.stderr.write('Unknown feature \'cat\' in FS: ' + str(self._fs))
                    sys.stderr.flush()
        #print(str(self._fs) + ' ' + nodeCat) #debug only
        if nodeCat == '':
            nodeCat = '-'
        return nodeCat

    def typerepr(self):
        if self._ntype == None:
            return ''
        elif self._ntype in ['lex','flex']:
            return ''
        elif self._ntype == 'foot':
            return u'\u2605' #black star
        elif self._ntype in ['anc', 'anchor']:
            return u'\u2666' #black diamond
        elif self._ntype in ['coanc', 'coanchor']:
            return u'\u25c7' #white diamond
        elif self._ntype == 'subst':
            return u'\u2193' #downarrow
        elif self._ntype == 'nadj':
            return u'\u266f' #sharp sign
        elif self._ntype == 'std':
            return ''
        else:
            print('Unknow node type: ' + self._ntype)
            return ''
    
    def childCount(self):
        return len(self._children)

    @property
    def color(self):
        return self._color
    
    def setColor(self, col):
        self._color = col

    def subColor(self, col):
        self._color = col
        for i in self._children:
            i.subColor(col)
        
    @property
    def children(self):
        return self._children

    @property
    def ntype(self):
        return self._ntype

    @property
    def name(self):
        return self._name

    @property
    def fs(self):
        return self._fs

    def child(self, i):
        try:
            return self._children[i]
        except IndexError:
            raise Exception("Unknown node")
    
    def __eq__(self, other):
        if self._ntype != other._ntype:
            return False
        else:
            return self._fs == other._fs

    def __repr__(self):
        repr_children = ",".join([repr(e) for e in self._children])
        return "%s<%s>" % (self._name + ' ' + str(self._fs), repr_children)

    def prettyprint(self):
        repr_children = ",".join([e.prettyprint() for e in self._children])
        if len(repr_children) > 0:
            return "%s(%s)" % (self.cat(), repr_children)
        else:
            return "%s" % (self.cat())

class FS(object):

    def __init__(self, feats):
        self.coref = None
        self.table = {}
        for f,v in feats:
            self.table[f] = v

    def prettyprint(self, avm, indent=0):
        s = ''
        for k in avm.table.keys():
            if k != 'cat': #cat already processed
                s += '  ' + '    ' * indent + k + ':' #always 2 leading ' '
                if isinstance(avm.table[k], str):
                    s += avm.table[k] + ';'
                elif isinstance(avm.table[k], ALT):
                    s += str(avm.table[k]) + ';'
                elif isinstance(avm.table[k], FS):
                    s += ';' + self.prettyprint(avm.table[k], indent+1) + ';' #',' are used by split in tree2svg
                else: #should not happen (cf dtd)
                    print('Unknown feature type: ' + str(avm.table[k]))
        return s

    def __eq__(self, other):
        if other != None:
            return self.table['cat'] == other.table['cat']
        else:
            return False
        
    def __repr__(self):
        s = '['
        for x in self.table.keys():
            s += x + ':' + str(self.table[x]) + ','
        s +=']'
        return s
