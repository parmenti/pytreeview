# -*- coding: utf-8 -*-
"""\
This module contains the entry point of the Desktop version of pytreeview.
It offers a graphical tool to search through a tree grammar and display 
specific entries.
Author: Yannick Parmentier
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pyforms, os, shutil, datetime
from   pyforms.basewidget import BaseWidget
from   pyforms.controls   import ControlText
from   pyforms.controls   import ControlButton
from   pyforms.controls   import ControlLabel
from   pyforms.controls   import ControlFile
from   pyforms.controls   import ControlCombo
from   pyforms.controls   import ControlList
from   pyforms.controls   import ControlDockWidget
from   pyforms.controls   import ControlProgress
from   sys                import exit
from   subprocess         import call, PIPE
from   loadXML            import XMLloader
from   loadFTB            import FTBloader
from   loadPSFTB          import PSFTBloader
from   loadXTAG           import XTAGloader
from   loadTiger          import Tigerloader
from   loadCoNLL          import CoNLLloader
from   grammar            import TAGrammar
from   TreeWindow         import TreeWindow

class mainGUI(BaseWidget):

    def __init__(self):
        super(mainGUI,self).__init__('Tree Viewer')

        #Properties
        self.grammar        = None    #init
        self._mode          = None    #XML,XMG,PYT
        self._enc           = 'utf-8' #default
        self._build         = None    #where to store images

        if 'PYTREEGUI_ENC' in os.environ:
            self._enc = os.environ['PYTREEGUI_ENC']
        
        #For standalone GUI use
        if 'PYTREE_HOME' not in os.environ:
            print('PYTREE_HOME NOT SET, PYTREEVIEWER MUST BE LAUNCHED FROM SOURCE DIR')
            self._build     = '.'
        else:
            self._build     = os.environ['PYTREE_HOME']
        if not os.path.isdir(os.path.join(self._build,'build')):
            os.mkdir(os.path.join(self._build,'build'))
            
        #Update build dir path
        timestamp = str(datetime.datetime.now()).replace(' ','-').replace(':','-')
        self._builddir_path = os.path.join(self._build,'build',timestamp)
        
        #Definition of the forms fields:
        self._xmlfile             = ControlFile('Select an XML file')
        self._mode                = 'XML' #default
        self._pytfile             = ControlFile('Select a PYT file')
        self._pytfile.hide()
        self._ftbfile             = ControlFile('Select a FTB file')
        self._ftbfile.hide()
        self._ptbfile             = ControlFile('Select a PTB file')
        self._ptbfile.hide()
        self._xmgfile             = ControlFile('Select a MG file')
        self._xmgfile.hide()
        self._xtagfile            = ControlFile('Select an XTAG file')
        self._xtagfile.hide()
        self._tigerfile           = ControlFile('Select an Tiger file')
        self._tigerfile.hide()
        self._conllfile           = ControlFile('Select a CoNLL file')
        self._conllfile.hide()        
        self._loadbutton          = ControlButton('Load grammar')
        self._gramload            = ControlLabel('')
        self._progload            = ControlProgress()
        self._progload.hide()
        self._searchcriteria      = ControlText('') 
        self._searchbutton        = ControlButton('Search')
        self._family              = ControlCombo('Family')
        self._displayFbutton      = ControlButton('Show family')
        self._trees               = ControlList('Entries')
        self._trees.horizontalHeaders = ['Entry name', 'Tree']
        self._trees.selectEntireRow   = True
        self._displayEbutton      = ControlButton('Show selected entry')
        self._displayDiff         = ControlButton('Diff selected entries')
        self._entrypanel          = ControlDockWidget(side='right')
        self._quitbutton          = ControlButton('Quit')

        #Define the button action:
        self._loadbutton.value    = self.__loadbuttonAction
        self._searchbutton.value  = self.__searchbuttonAction
        self._displayFbutton.value= self.__displayFbuttonAction
        self._displayEbutton.value= self.__displayEbuttonAction
        self._displayDiff.value   = self.__displayDiffAction        
        self._quitbutton.value    = self.__quitEvent
        
        #Define layout:
        #Use dictionaries for tabs
        #Use the sign '=' for a vertical splitter
        #Use the signs '||' for a horizontal splitter
        self._formset       = [ ('_xmlfile', '_pytfile', '_ftbfile', '_ptbfile', '_xmgfile', '_xtagfile', '_tigerfile', '_conllfile'), '_loadbutton', ('_gramload', '_progload'), ('_searchcriteria', '_searchbutton'), ('_family', '_displayFbutton'), '_trees', '_displayEbutton', '_displayDiff', ' ', '_quitbutton', '_entrypanel' ]
        
        #Define menus:
        self.mainmenu       = [
            { 'File': [
                {'Open XML'  : self.__openXMLEvent},
                {'Open FTB'  : self.__openFTBEvent},
                {'Open PTB'  : self.__openPTBEvent},
                {'Open XTAG' : self.__openXTAGEvent},
                {'Open Tiger': self.__openTigerEvent},
                {'Open CoNLL': self.__openCoNLLEvent},
                {'Load PYT'  : self.__loadEvent},
                {'Save PYT'  : self.__saveEvent},
                '-',
                {'Quit': self.__quitEvent}
            ]
            },
            { 'Compile': [
                {'Select MG': self.__openMGEvent}
            ]
            }
        ]

        #When GUI invoked from the pytreeview script:
        if 'PYTREE_GRAMMAR' in os.environ:
            if 'PYTREEGUI_MODE' in os.environ and os.environ['PYTREEGUI_MODE'] == 'PYT':
                self._xmlfile.hide()
                self._xmgfile.hide()
                self._ftbfile.hide()
                self._ptbfile.hide()
                self._pytfile.show()
                self._xtagfile.hide()
                self._tigerfile.hide()
                self._conllfile.hide()
                self._pytfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'PYT'
            elif 'PYTREEGUI_MODE' in os.environ and os.environ['PYTREEGUI_MODE'] == 'FTB':
                self._xmlfile.hide()
                self._xmgfile.hide()
                self._ftbfile.show()
                self._ptbfile.hide()                
                self._pytfile.hide()
                self._xtagfile.hide()
                self._tigerfile.hide()
                self._conllfile.hide()
                self._ftbfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'FTB'
            elif 'PYTREEGUI_MODE' in os.environ and os.environ['PYTREEGUI_MODE'] == 'PTB':
                self._xmlfile.hide()
                self._xmgfile.hide()
                self._ftbfile.hide()
                self._ptbfile.show()                
                self._pytfile.hide()
                self._xtagfile.hide()
                self._tigerfile.hide()
                self._conllfile.hide()
                self._ptbfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'PTB'
            elif 'PYTREEGUI_MODE' in os.environ and os.environ['PYTREEGUI_MODE'] == 'XTAG':
                self._xmlfile.hide()
                self._xmgfile.hide()
                self._ftbfile.hide()
                self._ptbfile.hide()
                self._pytfile.hide()
                self._xtagfile.show()
                self._tigerfile.hide()
                self._conllfile.hide()
                self._xtagfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'XTAG'
            elif 'PYTREEGUI_MODE' in os.environ and os.environ['PYTREEGUI_MODE'] == 'TIGER':
                self._xmlfile.hide()
                self._xmgfile.hide()
                self._ftbfile.hide()
                self._ptbfile.hide()
                self._pytfile.hide()
                self._xtagfile.hide()
                self._tigerfile.show()
                self._conllfile.hide()
                self._tigerfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'TIGER'
            elif 'PYTREEGUI_MODE' in os.environ and os.environ['PYTREEGUI_MODE'] == 'CONLL':
                self._xmlfile.hide()
                self._xmgfile.hide()
                self._ftbfile.hide()
                self._ptbfile.hide()
                self._pytfile.hide()
                self._xtagfile.hide()
                self._tigerfile.hide()
                self._conllfile.show()
                self._conllfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'CONLL'
            else:
                self._xmlfile.show()
                self._xmgfile.hide()
                self._ftbfile.hide()
                self._ptbfile.hide()                
                self._pytfile.hide()
                self._xtagfile.hide()
                self._tigerfile.hide()
                self._conllfile.hide()
                self._xmlfile.value   = os.environ['PYTREE_GRAMMAR']
                self._mode = 'XML'
            self._loadbutton.click()
        
    def __loadbuttonAction(self):
        """Load button action event"""
        #0. Reset (clear build/ dir):
        if os.path.isdir(self._builddir_path):
            for f in os.listdir(self._builddir_path):
                file_path = os.path.join(self._builddir_path, f)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                    elif os.path.isdir(file_path):
                        shutil.rmtree(file_path)
                except Exception as e:
                    print(e)
        else:
            os.mkdir(self._builddir_path)
        #1. Load grammar
        if self._mode == 'XML':
            self._progload.show()
            XMLfile      = self._xmlfile.value
            XMLreader    = XMLloader(XMLfile,self._enc)
            self.grammar = TAGrammar()
            XMLreader.getGrammar(self.grammar, self._progload)
            self._infile = os.path.basename(self._xmlfile.value)
            self._progload.hide()
        elif self._mode == 'FTB':
            self._progload.show()
            FTBfile      = self._ftbfile.value
            self.grammar = TAGrammar()
            if os.path.isdir(FTBfile):
                for infile in os.listdir(FTBfile):
                    filepath = os.path.join(FTBfile,infile)
                    if os.path.isfile(filepath):
                        FTBreader = FTBloader(filepath,self._enc) #iso-8859-1
                        FTBreader.getGrammar(self.grammar, self._progload)
            else: #FTBFile is indeed a file
                FTBreader = FTBloader(FTBfile,self._enc) #iso-8859-1
                FTBreader.getGrammar(self.grammar, self._progload)
            self._infile = os.path.basename(self._ftbfile.value)
            self._progload.hide()
        elif self._mode == 'PTB':
            self._progload.show()
            PTBfile      = self._ptbfile.value
            PTBreader    = PSFTBloader(PTBfile,self._enc)
            self.grammar = TAGrammar()
            PTBreader.getGrammar(self.grammar, self._progload)
            self._infile = os.path.basename(self._ptbfile.value)
            self._progload.hide()
        elif self._mode == 'PYT':
            PYTfile      = self._pytfile.value
            self.grammar = TAGrammar()
            self.grammar.load(PYTfile)
            self._infile = os.path.basename(self._pytfile.value)
        elif self._mode == 'XTAG':
            self._progload.show()
            XTAGfile      = self._xtagfile.value
            self.grammar = TAGrammar()
            if os.path.isdir(XTAGfile):
                for infile in os.listdir(XTAGfile):
                    filepath = os.path.join(XTAGfile,infile)
                    if os.path.isfile(filepath):
                        XTAGreader = XTAGloader(filepath,self._enc)
                        XTAGreader.getGrammar(self.grammar, self._progload)
            else: #XTAGFile is indeed a file
                XTAGreader = XTAGloader(XTAGfile,self._enc)
                XTAGreader.getGrammar(self.grammar, self._progload)
            self._infile = os.path.basename(self._xtagfile.value)
            self._progload.hide()
        elif self._mode == 'TIGER':
            self._progload.show()
            Tigerfile      = self._tigerfile.value
            self.grammar = TAGrammar()
            if os.path.isdir(Tigerfile):
                for infile in os.listdir(Tigerfile):
                    filepath = os.path.join(Tigerfile,infile)
                    if os.path.isfile(filepath):
                        reader = Tigerloader(filepath,self._enc)
                        reader.getGrammar(self.grammar, self._progload)
            else: #Tigerfile is indeed a file
                reader = Tigerloader(Tigerfile,self._enc)
                reader.getGrammar(self.grammar, self._progload)
            self._infile = os.path.basename(self._tigerfile.value)
            self._progload.hide()
        elif self._mode == 'CONLL':
            self._progload.show()
            Conllfile      = self._conllfile.value
            self.grammar = TAGrammar()
            if os.path.isdir(Conllfile):
                for infile in os.listdir(Conllfile):
                    filepath = os.path.join(Conllfile,infile)
                    if os.path.isfile(filepath):
                        reader = CoNLLloader(filepath,self._enc)
                        reader.getGrammar(self.grammar, self._progload)
            else: #Conllfile is indeed a file
                reader = CoNLLloader(Conllfile,self._enc)
                reader.getGrammar(self.grammar, self._progload)
            self._infile = os.path.basename(self._conllfile.value)
            self._progload.hide()
        elif self._mode == 'XMG':
            self._progload.show()
            MGfile       = self._xmgfile.value
            xmgcheck = call("type xmg", shell=True, stdout=PIPE, stderr=PIPE)
            if xmgcheck != 0:
                self._progload.value = 'Error: XMG2 not found'
            else:
                call(['xmg','compile','synsem', MGfile, '--force'])
                XMLfile      = os.path.splitext(self._xmgfile.value)[0]+'.xml'
                self._xmlfile.value = XMLfile
                XMLreader    = XMLloader(XMLfile,self._enc)
                self.grammar = TAGrammar()
                XMLreader.getGrammar(self.grammar, self._progload)
                self._infile = os.path.basename(self._xmgfile.value)
            self._progload.hide()
        else:
            print('Error: file type unknown') #should never happen
        ## Print grammar (for debug only):
        # for t in self.grammar.entries.keys():
        #     print(t)
        #     print(self.grammar.entries[t])
        #2. Update family widget:
        self._family.clear() #reset
        for i in range(len(sorted(self.grammar.families))):
            self._family.add_item(sorted(self.grammar.families)[i], str(i))
        self._gramload.value = self._infile + ' loaded'
        if len(self.grammar.families) > 0:
            self._family.text = sorted(self.grammar.families)[0] #first entry highlighted by default
            
    def __searchbuttonAction(self):
        """search button action event"""
        #0. Reload grammar
        self.__loadbuttonAction()
        #1. Search grammar
        if self._searchcriteria.value != '':
            self._progload.value = 0
            self._progload.show()
            allcrit = self._searchcriteria.value
            subgrammar = TAGrammar()
            self.grammar.filter(allcrit, subgrammar, self._progload)
            self.grammar = subgrammar
            self._progload.hide()
        #2. Update family widget:
        self._family.clear() #reset
        for i in range(len(sorted(self.grammar.families))):
            self._family.add_item(sorted(self.grammar.families)[i], str(i))
        self._gramload.value = self._infile + ' filtered'
        if len(self.grammar.families) > 0:
            self._family.text = sorted(self.grammar.families)[0] #first entry highlighted by default
        
    def __displayFbuttonAction(self):
        """display family button action event"""
        self._gramload.value = ''
        #Reset tree list widget:
        self._trees.clear()
        #Update tree list widget:
        self._progload.value = 0
        self._progload.max   = len(self.grammar.indexed[sorted(self.grammar.families)[int(self._family.value)]]) if self._family.value is not None else 0
        self._progload.show()
        #print(str(datetime.datetime.now())) #test only
        if self._family.value is not None:
            for t in self.grammar.indexed[sorted(self.grammar.families)[int(self._family.value)]]:
                self._progload.value += 1
                #print(t.tree.cat())
                #print(t.tree.prettyprint())
                self._trees.__add__([t.name, t.tree.prettyprint()])
        #print(str(datetime.datetime.now())) #test only
        self._progload.hide()
        if self._family.value is not None:
            self._gramload.value = 'family ' + sorted(self.grammar.families)[int(self._family.value)] + ' loaded'
        
    def __displayEbuttonAction(self):
        """display entry button action event"""
        self._gramload.value = ''
        #1. Get selected entry name:
        ename = self._trees.value[self._trees.selected_row_index][0] if self._trees.selected_row_index is not None else self._trees.value[0][0] #first entry by default
        #print(ename)
        #2. Get selected entry object:
        entry = self.grammar.entries[ename]
        #3. Reinit node color (case of diff)
        entry.tree.subColor('black')
        #4. Pass it onto the tree window:
        tw = TreeWindow()
        tw.setBuilddir(self._builddir_path)
        tw.hideLoadButton() #hide load button (cf standalone tests)
        tw.setEntry(entry)
        tw.fill() #fill tw fields (cf standalone tests)
        tw.parent = self
        self._entrypanel.value = tw

    def __displayDiffAction(self):
        """display diff button action event"""
        self._gramload.value = ''
        #1. Get selected entry names:
        if len(self._trees.selected_rows_indexes) > 1: #more than one item selected
            ename1= self._trees.value[self._trees.selected_rows_indexes[0]][0] 
            ename2 = self._trees.value[self._trees.selected_rows_indexes[1]][0]
            #print('tree1: '+str(ename))
            #print('tree2: '+str(ename2))
            #2. Get selected entry objects:
            entry1= self.grammar.entries[ename1]
            entry2= self.grammar.entries[ename2]
            #3. Reinit node colors (case of diff)
            entry1.tree.subColor('black')
            entry2.tree.subColor('black')
            #4. Update entry according to diff
            entry = entry1.basicDiff(entry2)
            #5. Pass it onto the tree window:
            tw = TreeWindow()
            tw.setBuilddir(self._builddir_path)
            tw.hideLoadButton() #hide load button (cf standalone tests)
            tw.setEntry(entry)
            tw.fill() #fill tw fields (cf standalone tests)
            tw.parent = self
            self._entrypanel.value = tw
        else:
            self._gramload.value = 'Please select 2 entries to diff'
        
    def __openXMLEvent(self):
        self._xmlfile.show()        
        self._xmgfile.hide()
        self._pytfile.hide()
        self._ftbfile.hide()
        self._ptbfile.hide()
        self._xtagfile.hide()
        self._tigerfile.hide()
        self._conllfile.hide()
        self._mode = 'XML'
        self._loadbutton.label = 'Load grammar'

    def __openFTBEvent(self):
        self._xmlfile.hide()
        self._xmgfile.hide()
        self._pytfile.hide()
        self._ftbfile.show()
        self._ptbfile.hide()
        self._xtagfile.hide()
        self._tigerfile.hide()
        self._conllfile.hide()
        self._mode = 'FTB'
        self._loadbutton.label = 'Load treebank'
        
    def __openPTBEvent(self):
        self._xmlfile.hide()
        self._xmgfile.hide()
        self._pytfile.hide()
        self._ftbfile.hide()
        self._ptbfile.show()
        self._xtagfile.hide()
        self._tigerfile.hide()
        self._conllfile.hide()
        self._mode = 'PTB'
        self._loadbutton.label = 'Load treebank'

    def __openXTAGEvent(self):
        self._xmlfile.hide()
        self._xmgfile.hide()
        self._pytfile.hide()
        self._ftbfile.hide()
        self._ptbfile.hide()
        self._xtagfile.show()
        self._tigerfile.hide()
        self._conllfile.hide()
        self._mode = 'XTAG'
        self._loadbutton.label = 'Load grammar'
        
    def __openTigerEvent(self):
        self._xmlfile.hide()
        self._xmgfile.hide()
        self._pytfile.hide()
        self._ftbfile.hide()
        self._ptbfile.hide()
        self._xtagfile.hide()
        self._tigerfile.show()
        self._conllfile.hide()
        self._mode = 'TIGER'
        self._loadbutton.label = 'Load treebank'

    def __openCoNLLEvent(self):
        self._xmlfile.hide()
        self._xmgfile.hide()
        self._pytfile.hide()
        self._ftbfile.hide()
        self._ptbfile.hide()
        self._xtagfile.hide()
        self._tigerfile.hide()
        self._conllfile.show()
        self._mode = 'CONLL'
        self._loadbutton.label = 'Load treebank'

    def __loadEvent(self):
        self._xmlfile.hide()
        self._xmgfile.hide()
        self._ftbfile.hide()
        self._ptbfile.hide()        
        self._pytfile.show()
        self._tigerfile.hide()
        self._conllfile.hide()
        self._mode = 'PYT'
        self._loadbutton.label = 'Load grammar'

    def __saveEvent(self):
        if self.grammar != None and self._xmlfile.value != '':
            self.grammar.save(os.path.splitext(self._xmlfile.value)[0] + '.pyt')
            self._gramload.value = os.path.basename(os.path.splitext(self._xmlfile.value)[0] + '.pyt') + ' saved'
        else:
            self._gramload.value = 'Save error: no grammar has been loaded'
            
    def __quitEvent(self):
        #clean build dir
        try:
            shutil.rmtree(self._builddir_path)
        except Exception as e:
            print(e)
        exit(0)

    def __openMGEvent(self):
        self._xmlfile.hide()
        self._pytfile.hide()
        self._ftbfile.hide()
        self._ptbfile.hide()
        self._xtagfile.hide()
        self._tigerfile.hide()
        self._conllfile.hide()
        self._xmgfile.show()
        self._mode = 'XMG'
        self._loadbutton.label = 'Compile and load grammar'
        
        
#Execute the application
if __name__ == "__main__":   pyforms.start_app( mainGUI, geometry=(200, 50, 600, 680) )
