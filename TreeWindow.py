# -*- coding: utf-8 -*-
"""\ 
This module contains the Tree Panel of the Desktop version of
pytreeview.  It relies on cairosvg and opencv (cv2.imshow()) to
resp. convert a tree (which has previously been converted in SVG) to
PNG and display it.  
Author: Yannick Parmentier
Date: 2017/01/23
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import pyforms, cairosvg, os, threading, subprocess
#from wand.image         import Image
from PIL import Image
from wand.display       import display
from pyforms.basewidget import BaseWidget
from pyforms.controls   import ControlButton
from pyforms.controls   import ControlLabel
from pyforms.controls   import ControlFile
from pyforms.controls   import ControlCheckBox
from pyforms.controls   import ControlList
from pyforms.controls   import ControlText
from pyforms.controls   import ControlSlider
from grammar            import TAGEntry

class TreeWindow(BaseWidget):

    def __init__(self):
        BaseWidget.__init__(self,'Entry window')
        self._name           = '' #init
        self._build          = 'build' #default value
        #widgets:
        self._svgfileSel     = ControlFile('Select a SVG file')
        self._loadbutton     = ControlButton('Display')
        self._traceL         = ControlLabel('Trace (XMG classes):')
        self._trace          = ControlList()#'Trace (XMG classes):')
        self._ifaceL         = ControlLabel('Interface:')
        self._iface          = ControlList()#ControlLabel()
        self._semL           = ControlLabel('Semantics:')
        self._sem            = ControlList()#ControlLabel()
        self._synL           = ControlLabel('Syntax:')
        self._wfeat          = ControlCheckBox('Display features')
        self._feats          = ControlText('Feats:')
        self._resize         = ControlSlider('Image size:',default=50,min=0,max=100)
        self._syn            = ControlButton('PNG')
        self._syntex         = ControlButton('Tikz')
        self._feats.value    = '(all)'
        #layout:
        self._formset = [ '_svgfileSel', '_loadbutton', '_traceL', '_trace', '_semL', '_sem', '_ifaceL', '_iface', ('_synL', '_wfeat'), '_feats', '_resize', ('_syn', '_syntex'), ' ']
        #actions:
        self._loadbutton.value = self.__loadbuttonAction
        self._syn.value        = self.__pngbuttonAction
        self._syntex.value     = self.__tikzbuttonAction
        #init
        self._entry   = None
        self._svgstr  = None
        self._tikzfile= None
        
        
    def setBuilddir(self, builddir):
        self._build = builddir
        
        
    def __loadbuttonAction(self):
        """Load button action event (only for test)"""
        with open(self._svgfileSel.value,'r') as f:
            svg = f.read()
        self.setSvgstr(svg)
        #print('* ' + self._svgfileSel.value)
        #print('**' + self._svgstr)
        self._name = 'test'
        self.getPNG()
        self.displaytree()

        
    def __pngbuttonAction(self):
        #0. Check whether some iface features are selected
        #   (to display them in red)
        indices= self._iface.selected_rows_indexes
        feats  = [self._iface.value[i] for i in indices]
        toshow = []
        for f in feats:
            toshow.append(''.join(f).split(':')[1])
        #   and restrict feature display to those declared
        ffilter = []
        ffeats  = self._feats.value
        if ffeats != '(all)' and ffeats != '':
            for fe in ffeats.split(';'):
                ffilter.append(fe.strip())
        #1. Create SVG file (featOn check in subroutine)
        self.getSVG(toshow, ffilter)
        #2. Create PNG file
        self.getPNG()
        #3. Display tree
        self.displaytree()

        
    def __tikzbuttonAction(self):
        #1. Create SVG file (featOn check in subroutine)
        self.getSVG([],[]) #empty list for all feat identically considered, and no filter
        #2. Create TIKZ file
        self.getTIKZ()
        #3. Display TIKZ file
        if self._tikzfile is not None:
            os.system('xdg-open ' + self._tikzfile)

    
    def setEntry(self, entry):
        self._entry = entry
        self._name  = entry.name

        
    def fill(self):
        classes = list()
        for c in sorted(self._entry.trace):
            classes += [[c]]
        self._trace.value = classes
        #self._iface.value = '\n'.join([s for s in str(self._entry.iface).split(',')])
        #self._sem.value   = self._entry.semantics
        interface = list()
        for i in str(self._entry.iface).replace('[','').replace(']','').split(','):
            if i != '':
                interface += [[i]]
        self._iface.value = sorted(interface)
        semantics = list()
        for s in list(map(lambda x : str(x),self._entry.semantics)):
            if s != '':
                semantics += [[s]]
        self._sem.value = semantics
        
        
    def keyatright(self, s): #pretty print function (unused, cf no monospace font)
        if ':' in s :
            l   = s.split(':')
            k,v = l[0],l[1]
            return '{:_<10}'.format(k) + ':' '{:_<10}'.format(v)
        else:
            return s

        
    def getSvgstr(self):
        return self._svgstr

    
    def setSvgstr(self, string):
        self._svgstr = string

        
    def hideLoadButton(self):
        self._svgfileSel.hide()
        self._loadbutton.hide()

        
    def getSVG(self, feats, ffilter):
        prefix = os.environ['PYTREE_HOME'] if 'PYTREE_HOME' in os.environ else ''
        if self._entry != None:
            self._svgstr = self._entry.tree2svg(self._wfeat.value, feats, ffilter)

            
    def getPNG(self):
        prefix = os.environ['PYTREE_HOME'] if 'PYTREE_HOME' in os.environ else ''
        #Convert SVG to PNG
        if self._svgstr != None:
            self._pngfile = os.path.join(prefix, self._build, self._name + '.png')
            cairosvg.svg2png(bytestring=self._svgstr, write_to=self._pngfile)

            
    def getTIKZ(self):
        #Convert SVG to TIKZ
        prefix = os.environ['PYTREE_HOME'] if 'PYTREE_HOME' in os.environ else ''
        if self._svgstr != None and self._entry != None:
            self._tikzfile = os.path.join(prefix, self._build, self._name + '.tex')
            self._entry.treesvg2tikz(self._svgstr, self._tikzfile)

            
    def resize(self, ratio):
        #Set image size and display it
        with Image.open(self._pngfile) as img:
            img = img.resize((int(float(img.size[0]) * ratio),int(float(img.size[1])*ratio)), Image.ANTIALIAS)
            img.save(self._pngfile + '.in.png')
            # with img.clone() as i:
            #     i.resize(int(i.width * ratio), int(i.height * ratio))
            #     i.save(filename=self._pngfile + '.in')
            #     #display(i) #wand's display replaced with
            #     #subprocess.run(["display", self._pngfile+'.in'], stdout=subprocess.PIPE)
            subprocess.call(["display", self._pngfile+'.in.png'], stdout=subprocess.PIPE)


    def displaytree(self):
        #Display tree in PNG
        #print('displaying ... '+ self._pngfile)
        ratio = (self._resize.value + 50) / 100  #+50 cause the default value is 50 (real size)
        #self.resize(ratio) #replaced with a thread (for continuing family update)
        t = threading.Thread(target=self.resize,args=(ratio,))
        t.daemon = True #to detach the thread
        t.start()


#Execute the application
if __name__ == "__main__":   pyforms.startApp( TreeWindow, geometry=(500, 200, 400, 400) )
