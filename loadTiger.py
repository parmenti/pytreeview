# -*- coding: utf-8 -*-
"""\ 
This module reads syntactic trees encoded in XML format from the Tiger TreeBank.
It outputs objects of type TAGEntries so as to be displayed by pytreeview. 
Author: Yannick Parmentier 
Date: 2017/03/13
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import json, sys, os, traceback
from grammar  import TAGEntry
from lxml     import etree

class Tigerloader(object):
    
    def __init__(self, filename, encoding):
        self.xmlfile = filename
        self.encoding= encoding
        self.suffix  = 1 #for creating unique names

    def getGrammar(self, g, progload=None):
        parser     = etree.XMLParser(remove_comments=True, encoding=self.encoding)
        try:
            xmldoc = etree.parse(self.xmlfile, parser = parser)
            nbSent = xmldoc.xpath('count(//s)')
            if progload is not None:
                if 'printProgressBar' in dir(progload):
                    progload.setTotal(nbSent)
                else: #pyForms bar
                    progload.max = nbSent
            for sentence in xmldoc.findall(".//s"): 
                tage = self.getEntry(sentence)  #tage here is a full syntactic tree
                if progload is not None:
                    if 'printProgressBar' in dir(progload):
                        progload.value += 1
                        progload.printProgressBar()
                    else: #pyForms bar
                        progload.value += 1
                g.addEntry(tage)
        except etree.XMLSyntaxError:
            traceback.print_stack()
            raise Exception("XML syntax error")

    def getEntry(self,xmlentry):
        tname      = xmlentry.get('id') + '_' + str(self.suffix) + '_' + os.path.splitext(os.path.basename(self.xmlfile))[0]
        tfamily    = 'TB'
        ttrace     = []
        tsemantics = ''
        xmlfs      = None 
        tiface     = None
        ttree      = getNode(xmlentry)
        self.suffix= self.suffix + 1 #not used for now
        return TAGEntry(tname,tfamily,ttrace,ttree,tsemantics,tiface)

def getNode(xmlnode):
        where = str(xmlnode.tag)
        #print(where + ' ' + str(xmlnode.items())) #debug only
        if where == 's': #we extract the root node id
            return getNode(xmlnode.find('graph'))
        elif where == 'graph': #we process the root and get its children
            nodeId   = xmlnode.get('root')
            nextNode = xmlnode.findall('nonterminals/nt[@id=\"'+nodeId+'\"]')
            if len(nextNode) == 0: #in case the root points to a single terminal
                nextNode = xmlnode.findall('terminals/t[@id=\"'+nodeId+'\"]')
            return getNode(nextNode[0])
        elif where == 'nt':
            return Node(xmlnode)
        elif where == 't':
            return Node(xmlnode)
        elif where=='edge':
            target = xmlnode.get('idref')
            label  = xmlnode.get('label')
            newnode= xmlnode.getparent().getparent().findall('../..//*[@id=\"'+target+'\"]')[0]
            if label != '':
                newnode.attrib['label'] = label
            return getNode(newnode)
    
class Node(object):

    def __init__(self, xmlnode):
        self._color = 'black' #for tree diff
        where = str(xmlnode.tag)
        #where should be either nt or t (cf getNode function)
        if where == 'nt':
            self._name  = xmlnode.get('id')
            self._ntype = 'std'
            fs = [('cat',xmlnode.get('cat'))]
            if xmlnode.items() is not None:
                for k,v in xmlnode.items():
                    if k != 'id':
                        fs.append((k,v))
            self._fs = FS(fs)
            self._children = tuple(map(getNode,list(xmlnode.findall('edge'))))
        elif where == 't':
            self._name  = xmlnode.get('id')
            self._ntype   = 'lex'
            fs = [('cat',xmlnode.get('word'))]
            if xmlnode.items() is not None:
                for k,v in xmlnode.items():
                    if k != 'id':
                        fs.append((k,v))
            self._fs = FS(fs)
            self._children= ()
            
    def cat(self):
        nodeCat = ''
        try:
            nodeCat = str(self._fs.table['cat'])
        except KeyError:
            sys.stderr.write('Unknown feature \'cat\' in FS: ' + str(self._fs))
            sys.stderr.flush()
        #print(str(self._fs) + ' ' + nodeCat) #debug only
        if nodeCat == '':
            nodeCat = '-'
        return nodeCat

    def typerepr(self):
        if self._ntype == None:
            return ''
        elif self._ntype in ['lex','flex']:
            return ''
        elif self._ntype == 'std':
            return ''
        else:
            print('Unknow node type: ' + self._ntype)
            return ''
    
    def childCount(self):
        return len(self._children)

    @property
    def color(self):
        return self._color
    
    def setColor(self, col):
        self._color = col

    def subColor(self, col):
        self._color = col
        for i in self._children:
            i.subColor(col)
        
    @property
    def children(self):
        return self._children

    @property
    def ntype(self):
        return self._ntype

    @property
    def name(self):
        return self._name

    @property
    def fs(self):
        return self._fs

    def child(self, i):
        try:
            return self._children[i]
        except IndexError:
            raise Exception("Unknown node")
    
    def __eq__(self, other):
        if self._ntype != other._ntype:
            return False
        else:
            return self._fs == other._fs

    def __repr__(self):
        repr_children = ",".join([repr(e) for e in self._children])
        return "%s<%s>" % (self._name + ' ' + str(self._fs), repr_children)

    def prettyprint(self):
        repr_children = ",".join([e.prettyprint() for e in self._children])
        if len(repr_children) > 0:
            return "%s(%s)" % (self.cat(), repr_children)
        else:
            return "%s" % (self.cat())

class FS(object):

    def __init__(self, feats):
        self.coref = None
        self.table = {}
        for f,v in feats:
            self.table[f] = v

    def prettyprint(self, avm, indent=0):
        s = ''
        for k in avm.table.keys():
            if k != 'cat': #cat already processed
                s += '  ' + '    ' * indent + k + ':' #always 2 leading ' '
                if isinstance(avm.table[k], str):
                    s += avm.table[k] + ';'
                # elif isinstance(avm.table[k], ALT):
                #     s += str(avm.table[k]) + ';'
                elif isinstance(avm.table[k], FS):
                    s += ';' + self.prettyprint(avm.table[k], indent+1) + ';' #',' are used by split in tree2svg
                else: #should not happen (cf dtd)
                    print('Unknown feature type: ' + str(avm.table[k]))
        return s

    def __eq__(self, other):
        if other != None:
            return self.table['cat'] == other.table['cat']
        else:
            return False
        
    def __repr__(self):
        s = '['
        for x in self.table.keys():
            s += x + ':' + str(self.table[x]) + ','
        s +=']'
        return s
