# -*- coding: utf-8 -*-
"""\
This module contains the processing of tigerSearch-like querys on trees.
Author: Yannick Parmentier
Date: 2017/04/03
"""
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

import sys, uuid, copy
from lark                  import Lark, Transformer, common, lexer

class qNode:

    def __init__(self):
        self._id = ''
        self._feats    = {}
        self._dom      = []
        self._largedom = []
        self._prec     = []
        self._largeprec= []
        self._domn     = []
        self._dommn    = []
        self._precn    = []
        self._precmn   = []
        self._sibling  = []
        self._sister   = []
        self._sibprec  = []
        self._leftmost = []
        self._rightmost= []

    def setId(self, s):
        self._id = s

    def addFeats(self, feats):
        for k,v in feats.items():
            self._feats[k] = v

    def addDom(self, r):
        self._dom.append(r)

    def addLargedom(self, r):
        self._largedom.append(r)

    def addPrec(self, r):
        self._prec.append(r)

    def addLargeprec(self, r):
        self._largeprec.append(r)

    def addDomn(self, r):
        self._domn.append(r)

    def addDommn(self, r):
        self._dommn.append(r)

    def addPrecn(self, r):
        self._precn.append(r)

    def addPrecmn(self, r):
        self._precmn.append(r)

    def addSibling(self, r):
        self._sibling.append(r)

    def addSister(self, r):
        self._sister.append(r)

    def addSibprec(self, r):
        self._sibprec.append(r)

    def addLeftmost(self,r):
        self._leftmost.append(r)
        
    def addRightmost(self,r):
        self._rightmost.append(r)
        
    @property
    def id(self):
        return self._id

    @property
    def feats(self):
        return self._feats
        
    @property
    def dom(self):
        return self._dom

    @property
    def largedom(self):
        return self._largedom

    @property
    def prec(self):
        return self._prec

    @property
    def largeprec(self):
        return self._largeprec

    @property
    def domn(self):
        return self._domn

    @property
    def dommn(self):
        return self._dommn

    @property
    def precn(self):
        return self._precn

    @property
    def precmn(self):
        return self._precmn

    @property
    def sibling(self):
        return self._sibling

    @property
    def sister(self):
        return self._sister

    @property
    def sibprec(self):
        return self._sibprec

    @property
    def leftmost(self):
        return self._leftmost

    @property
    def rightmost(self):
        return self._rightmost

    def __repr__(self):
        s = self.id
        s += '[' 
        for k,v in self.feats.items():
            s+= k+'='+str(v)+','
        s += '] '
        for n2 in self.dom:
            s+= self.id + ' > ' + n2[1] + ' '
        for n2 in self.largedom:
            s+= self.id + ' >* ' + n2[1] + ' '        
        for n2 in self.prec:
            s+= self.id + ' . ' + n2[1] + ' '
        for n2 in self.largeprec:
            s+= self.id + ' .* ' + n2[1] + ' '
        for n2 in self.domn:
            s+= self.id + ' >n ' + n2[1] + '(' + str(n2[2]) + ') '
        for n2 in self.dommn:
            s+= self.id + ' >m,n ' + n2[1] + '(' + str(n2[2][0]) + ',' + str(n2[2][1]) + ') '
        for n2 in self.precn:
            s+= self.id + ' .n ' + n2[1] + '(' + str(n2[2]) + ') '
        for n2 in self.precmn:
            s+= self.id + ' .m,n ' + n2[1] + '(' + str(n2[2][0]) + ',' + str(n2[2][1]) + ') '
        for n2 in self.sibling:
            s+= self.id + ' $ ' + n2[1] + ' '
        for n2 in self.sister:
            s+= self.id + ' $. ' + n2[1] + ' '
        for n2 in self.sibprec:
            s+= self.id + ' $.* ' + n2[1] + ' '            
        for n2 in self.leftmost:
            s+= self.id + ' >@l ' + n2[1] + ' '
        for n2 in self.rightmost:
            s+= self.id + ' >@r ' + n2[1] + ' '            
        return s

class QueryTransformer(Transformer):

    def query(self, items):
        return items

    def rel(self, rel):
        return rel

    def disj(self, disj):
        return ('disj', disj[0], disj[1])

    def value(self, value):
        return value

    def node(self, node):
        return ('node',node[0])

    def avm(self, avm):
        return dict(avm)
    
    def pair(self, pair):
        #print(pair[0].__class__.__name__)
        return pair[0], pair[1]

    def dom(self, dom):
        return ('>',dom[0],dom[1])

    def largedom(self, domplus):
        return ('>*',domplus[0],domplus[1])

    def prec(self, prec):
        return ('.',prec[0],prec[1])

    def largeprec(self, precplus):
        return ('.*',precplus[0],precplus[1])

    def domn(self, domnrel):
        return ('>n', domnrel[0], domnrel[2], domnrel[1]) #number in last arg

    def dommn(self, dommnrel):
        return ('>m,n', dommnrel[0], dommnrel[3], (dommnrel[1], dommnrel[2]))

    def precn(self, precnrel):
        return ('.n', precnrel[0], precnrel[2], precnrel[1]) #number in last arg

    def precmn(self, precmnrel):
        return ('.m,n', precmnrel[0], precmnrel[3], (precmnrel[1],precmnrel[2])) 

    def siblings(self, sibrel):
        return ('$', sibrel[0], sibrel[1]) 
    
    def sister(self, sisrel):
        return ('$.', sisrel[0], sisrel[1]) 

    def siblingsprec(self, sibplusrel):
        return ('$.*', sibplusrel[0], sibplusrel[1])

    def leftmost(self, rel):
        return ('>@l', rel[0], rel[1])

    def rightmost(self, rel):
        return ('>@r', rel[0], rel[1])
    
    def regexp(self, regexp):
        return regexp[0][:]
    
    def name(self,n):
        return n[0].value

    def string(self, s):
        return s[0].value[1:-1] #to remove \"\"

    def number(self, n):
        return int(n[0].value)
    
class QueryProcessor(object):

    def __init__(self):
        self._accu    = [dict()] #list of constraint sets (one dict per constraint set)
        self._iast    = None     #transformed AST
        
        #parser spec for home queries
        self._grammar = r"""
        query : [rel ("&" rel)*]

        rel : value
        | dom
        | largedom
        | domn
        | dommn
        | prec
        | largeprec
        | precn
        | precmn
        | leftmost
        | rightmost
        | siblings
        | sister
        | siblingsprec
        | disj

        disj : "(" [rel ("|" rel)*] ")"

        value : node
        | avm
        | node ":" avm
        | ESCAPED_STRING -> string
        | "{" [value ("|" value)*] "}" 

        node : "#" name

        avm : "[" [pair ("&" pair)*] "]"
        pair : name "=" value
        | name "=" regexp 

        name : NAME
        regexp : REGEXP
        REGEXP : "/" (LETTER|".") (LETTER|DIGIT|"."|"+"|"*")* "/"

        dom : value ">" value
        largedom : value ">*" value
        domn : value ">" number value
        dommn : value ">" number "," number value
        leftmost : value ">@l" value
        rightmost : value ">@r" value
        prec : value "." value
        largeprec : value ".*" value
        precn : value "." number value
        precmn : value "." number "," number value
        siblings : value "$" value
        sister : value "$." value
        siblingsprec : value "$.*" value

        number : INT

        %import common.CNAME -> NAME
        %import common.ESCAPED_STRING
        %import common.INT
        %import common.LETTER
        %import common.DIGIT
        %import common.WS
        %ignore WS
        """
        self._parser = Lark(self._grammar, start='query', lexer='standard', parser='lalr', transformer=QueryTransformer())

    @property
    def accu(self):
        return self._accu

    @property
    def iast(self):
        return self._iast

    def addNode(self, const_set, node_id):
        if node_id not in const_set.keys():
            const_set[node_id] = qNode()
            const_set[node_id].setId(node_id)
    
    def parseQuery(self, query):
        self._iast= self._parser.parse(query) 
        #print(self._iast) #print Interpreted Abstract Syntax Tree (debug only)
        for instruction in self._iast:     #NB: instructions in the list are interpreted conjunctively
            #print(instruction[0])         #[0] cause each instruction is a list of lists 
            current_sets=self._accu[:]     #we need to work on a (shallow) copy so that the same instruction is not applied several times
            for const_set in current_sets: #current environment (set where to store constraints)
                self.parseInst(instruction[0], const_set)

    def parseInst(self, inst, const_set, node_id=None):
        #print(inst) #debug only
        ## CASE 1: instruction is a node definition
        if isinstance(inst, list):
            head = inst[0]
            node = node_id #default
            if isinstance(head,dict): #avm (anonymous node)
                if node_id == None:
                    node = str(uuid.uuid4())
                    self.addNode(const_set,node)
                const_set[node].addFeats(head)
            elif head[0] == 'node': #head == ('node', id)
                node = head[1]
                self.addNode(const_set,node)
                if len(inst) > 1: #head comes with some features
                    tail = inst[1:]
                    feats= tail[0]
                    const_set[node].addFeats(feats)
        ## OTHERWISE
        elif isinstance(inst, tuple):
            reltype = inst[0]
            ## CASE 2: instruction is a node relation
            if reltype in ['>','>*','.','.*','>n','>m,n','.n','.m,n','$','$.','$.*','>@l','>@r']:
                node1   = inst[1]
                node2   = inst[2]
                if isinstance(node1[0],dict): #anonymous node (feat constraints only)
                    node1_id= str(uuid.uuid4()) #to get a unique id 
                    self.addNode(const_set,node1_id)
                else:
                    node1_id= node1[0][1]
                if isinstance(node2[0],dict):
                    node2_id= str(uuid.uuid4())
                    self.addNode(const_set,node2_id)
                else:
                    node2_id= node2[0][1]
                self.parseInst(node1, const_set, node1_id)
                self.parseInst(node2, const_set, node2_id)
                if reltype == '>':
                    const_set[node1_id].addDom((node1_id, node2_id, None))
                elif reltype == '>*':
                    const_set[node1_id].addLargedom((node1_id, node2_id, None))
                elif reltype == '.':
                    const_set[node1_id].addPrec((node1_id, node2_id, None))
                elif reltype == '.*':
                    const_set[node1_id].addLargeprec((node1_id, node2_id, None))
                elif reltype == '>n':
                    const_set[node1_id].addDomn((node1_id, node2_id, inst[3]))
                elif reltype == '>m,n':
                    const_set[node1_id].addDommn((node1_id, node2_id, inst[3]))
                elif reltype == '.n':
                    const_set[node1_id].addPrecn((node1_id, node2_id, inst[3]))
                elif reltype == '.m,n':
                    const_set[node1_id].addPrecmn((node1_id, node2_id, inst[3]))
                elif reltype == '$':
                    const_set[node1_id].addSibling((node1_id, node2_id, None))
                elif reltype == '$.':
                    const_set[node1_id].addSister((node1_id, node2_id, None))
                elif reltype == '$.*':
                    const_set[node1_id].addSibprec((node1_id, node2_id, None))
                elif reltype == '>@l':
                    const_set[node1_id].addLeftmost((node1_id, node2_id, None))
                elif reltype == '>@r':
                    const_set[node1_id].addRightmost((node1_id, node2_id, None))
            ## CASE 3: instruction is a disjunction
            elif reltype == 'disj':
                inst1 = inst[1][0]
                inst2 = inst[2][0]
                const_set2 = copy.deepcopy(const_set)
                #we copy the constraint set and update each of these
                self.parseInst(inst1, const_set)
                self.parseInst(inst2, const_set2)
                #we store all constraint sets together
                self._accu.append(const_set2)


if __name__ == "__main__":
    if len(sys.argv) == 1:
        query = input('Query ? ')
        while query != 'q':
            q = QueryProcessor()
            try:
                q.parseQuery(query)
                print('------')
                print(q.iast)
                print('------')
                for a in q.accu:
                    for n in a.keys():
                        print(a[n])
                    print('------')
            except common.UnexpectedToken as e:
                print('Ill-formed query, please check your query', file=sys.stderr)
                print(e)
            except lexer.UnexpectedInput as e:
                print('Ill-formed query, please check your query', file=sys.stderr)
                print(e)
            query = input('Query ? ')
    elif len(sys.argv) >= 1:
        testfile = open(sys.argv[1], 'r')
        queries = testfile.readlines()
        for qu in queries:
            if len(qu) > 0 and qu[0] != '%':
                q = QueryProcessor()
                try:
                    q.parseQuery(qu)
                    iast = q.iast
                except Exception as e:
                    print('Ill-formed query, please check your query', file=sys.stderr)
                    print(e)
            
